$(document).ready(function() {

    // fake select
    $('.booking-block__select').on('click', function(e) {
        e.stopPropagation()
    })
    $('.booking-block__select').on('click', '.placeholder', function() {
        var parent = $(this).closest('.booking-block__select');
        if (!parent.hasClass('is-open')) {
            parent.addClass('is-open');
            $('.booking-block__select.is-open').not(parent).removeClass('is-open');
        } else {
            parent.removeClass('is-open');
        }
    }).on('click', 'ul>li', function() {
        var parent = $(this).closest('.booking-block__select');
        $(this).addClass('active').siblings().removeClass('active');
        parent.removeClass('is-open').find('.placeholder__text').text($(this).find('.select-list__item').text());
        parent.find('input[type=hidden]').attr('value', $(this).attr('data-value'));
    });

    // btn choosen
    $('.btn--choose').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('choosed')
        if ($(this).hasClass('choosed')) {
            $(this).find('.btn-choose__text').text('Выбор сделан')
        } else {
            $(this).find('.btn-choose__text').text('Выбрать')
        }
    })

    // show booking btn
    $('html').on('click', function() {
        $('.booking-block__select').removeClass('is-open')
        if ($('.btn--choose').hasClass('choosed')) {
            $('.btn-to-booking').addClass('show')
        } else {
            $('.btn-to-booking').removeClass('show')
        }
    })

})